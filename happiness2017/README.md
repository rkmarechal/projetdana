# Dataset : World Happiness 2017 (only)

Toutes les requêtes ici concerne uniquement le "*World Happiness* pour l'année 2017. Voir [ici]() pour le dataset concernant les années 2015, 2016 et 2017.

* [Dataset format au format CVS](https://github.com/RoxaneKM/web-semantic-project/blob/master/2017.csv)
* [Dataset au format ttl](https://github.com/RoxaneKM/web-semantic-project/blob/master/happiness.ttl)
* [Mapping sparql](https://gitlab.com/RoxaneKM/web-semantic-project/blob/master/mapping.rq)